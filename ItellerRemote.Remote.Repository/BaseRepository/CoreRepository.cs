﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository;
using ItellerRemote.Remote.Repository.ViewModels;
using ItellerRemote.Service.Core;

namespace ItellerRemote.Remote.Repository.BaseRepository
{
    public class CoreRepository : ICoreRepository
    {
        public CoreRepository(ITellerRemoteDBContext context)
        {
            _context = context;
        }

        private readonly ITellerRemoteDBContext _context;

        public async Task<UserViewModel> GetUserByUsername(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                    throw new Exception("Invalid username");

                var objUser = await _context.Users.Where(x => x.UserName == username)
                    .Select(x => new UserViewModel
                    {
                        Username = x.Email,
                        UserId = x.Id,
                        UserIsInRoles = x.Roles.Where(y => y.UserId == x.Id).Select(y => new UserRoles
                        {
                            RoleId = y.RoleId,
                            RoleName = y.Role.Name
                        })
                            .ToList(),

                        UserIsInBranches = x.UserBranches.Where(y => y.Security_UsersId == x.Id).Select(y => new UserBranches
                        {
                            BranchId = y.ApplicationSetup_BranchId,
                            BranchName = y.ApplicationSetup_Branch.Name
                        })
                            .ToList(),
                    })
                    .FirstOrDefaultAsync();
                if (objUser == null)
                    throw new Exception("Invalid User");


                //var objUserRoles = await _context.Security_UserRoles.Where(y => y.UserId == objUser.UserId)
                //    .Select(y => new UserRoles
                //    {
                //        UserId = y.UserId,
                //        RoleId = y.RoleId
                //    })
                //    .ToListAsync();

                //var listRoles = new List<UserRoles>();

                //var listBranches = new List<UserBranches>();

                //Parallel.ForEach(objUserRoles, role =>
                //{
                //    var userRoles = new UserRoles();

                //    userRoles.UserId = role.UserId;
                //    userRoles.RoleId = role.RoleId;

                //    listRoles.Add(userRoles);
                //});

                //var objUserBranches = await _context.Security_UserBranches.Where(y => y.UserId == objUser.UserId)
                //    .Select(y => new UserBranches
                //    {
                //        UserId = y.UserId,
                //        BranchId = y.BranchId
                //    })
                //    .ToListAsync();
                //Parallel.ForEach(objUserBranches, branch =>
                //{

                //    var userBranches = new UserBranches();
                //    userBranches.UserId = branch.UserId;
                //    userBranches.BranchId = 
                //});

                //objUser.UserIsInRoles = listRoles;

                return objUser;
            }
            catch (Exception ex)
            {
                Console.WriteLine();
                throw ex;
            }
        }
    }
}
