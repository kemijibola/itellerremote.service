﻿using DryIoc;
using ItellerRemote.Remote.Repository.BaseRepository;
using ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository;
using ItellerRemote.Service.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace ItellerRemote.Remote.Repository.ItellerConfig
{
    public static class IoCDependencyResolver : IModule
    {
        private static readonly Assembly CurrentAssembly = typeof(ItellerRemote.Service.MvcApplication).Assembly;
        private static readonly Type ApiControllerType = typeof(ApiController);

        public static void RegisterDependencies(this IContainer container)
        {
            container.RegisterRepositories();

            container.RegisterServices();

            container.RegisterDbContexts();

            container.RegisterControllers();
        }

        private static void RegisterControllers(this IContainer container)
        {
            foreach (Type controller in CurrentAssembly.GetTypes().Where(t => !t.IsAbstract && t.IsClass && ApiControllerType.IsAssignableFrom(t)))
            {
                container.Register(controller, Reuse.InResolutionScope);
            }
        }

        private static void RegisterRepositories(this IContainer container)
        {
            // register repos eg.
            container.Register(typeof(IRepository<>), typeof(Repository<>));
        }

        private static void RegisterServices(this IContainer container)
        {
            // register services eg.
            container.Register<IAppStartup, AppStartup>(Reuse.Singleton);
        }

        private static void RegisterDbContexts(this IContainer container)
        {
            // register DbContexs eg.
            container.Register<ITellerRemoteDBContext>(made: Made.Of(() => new ITellerRemoteDBContext()), reuse: Reuse.Singleton);
        }
    }
}
