﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItellerRemote.Remote.Repository.ViewModels;

namespace ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository
{
    public interface IApplicationSetUpBranch
    {
        //Task<List<ApplicationSetupBranchViewModel>> GetBranches();
        Task<List<ApplicationSetupBranchViewModel>> GetBranchesAsync();
        //Task<ApplicationSetupBranchViewModel> GetBranchById(int branchId);
        //Task<ApplicationSetupBranchViewModel> GetBranchByIdAsync(int branchId);
        Task<bool> CreateBranch(ApplicationSetupBranchViewModel applicationSetupBranchViewModel);
    }
}
