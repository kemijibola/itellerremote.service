﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItellerRemote.Service.Core;

namespace ItellerRemote.Remote.Repository.ViewModels
{
    public class RegisterUserViewModel 
    {
        [Required(ErrorMessage = "Username is required")]
        [Display(Name = "Username")]
        //[System.Web.Mvc.Remote("UsernameAlreadyExistsAsync", "Account", ErrorMessage = "User name already exists.")]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        //[System.Web.Mvc.Remote("EmailAlreadyExistsAsync", "Account", ErrorMessage = "Email already exists.")]
        public string Email { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name="Phone number")]
        [Required]
        public string PhoneNumber { get; set; }

    }
}
