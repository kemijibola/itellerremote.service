﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository;
using ItellerRemote.Remote.Repository.ViewModels;
using ItellerRemote.Service.Controllers.BaseController;
using ItellerRemote.Service.ItellerConfig;

namespace ItellerRemote.Service.Controllers
{
    [RoutePrefix("api/account")]
    //[Authorize]
    public class UserController : ITellerBaseController
    {
        public UserController(UserViewModel currentUser, IUserRepository userRepository)
            :base(currentUser)
        {
            CurrentUser = currentUser;
            //currentUserID = currentUser.UserId;
            //currentUser = CurrentUser.UserId
            _userRepository = userRepository;
        }

       // private UserViewModel _currentUser;
        private int currentUserID = 0;
        private readonly IUserRepository _userRepository;

        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> Create(RegisterUserViewModel newUser)
        {
            //if (User.Identity.IsAuthenticated)
            //{
            //    if (ModelState.IsValid)
            //    {
            //        var result = await _userRepository.CreateUser(newUser);
            //        var response = Request.CreateResponse(HttpStatusCode.OK, result);
            //        return ResponseMessage(HttpStatusCode.OK, "01", "Successful", response);
            //    }
            //    return ResponseMessage(HttpStatusCode.OK, "00", "Fields are required.", null);
            //}
            //else
            //    return ResponseMessage(HttpStatusCode.OK, "00", "You are not authorized to view this resource.", null);


                if (ModelState.IsValid)
                {
                    var result = await _userRepository.CreateUser(newUser);
                    
                    //var response = Request.CreateResponse(HttpStatusCode.OK, result);
                    return ResponseMessage(HttpStatusCode.OK, "01", "Successful", result);
                }
                return ResponseMessage(HttpStatusCode.OK, "00", "Fields are required.", null);

        }

    }
}
