﻿using DryIoc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItellerRemote.Service.ItellerConfig
{
    public class Config
    {

        //public Config(IContainer container)
        //{
        //    _container = container;
        //}

        //private static IContainer _container;
        static readonly string serverIp = GetConfigValue("Server");
        static readonly string dbName = GetConfigValue("DBName");
        static readonly string userId = GetConfigValue("UserID");
        static readonly string password = GetConfigValue("Password");

        public string ConnectionString()
        {
                return GenerateConnectionstring(serverIp, dbName, userId, password);            
        }

        private static string GetConfigValue(string _key)
        {
            return ConfigurationManager.AppSettings[_key];
        }

        private static string GenerateConnectionstring(string dbServerIP, string databaseName, string userId, string hashedPassword)
        {
            var builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
            builder["Server"] = dbServerIP;
            builder["integrated Security"] = false;
            builder["Database"] = databaseName;
            builder["User Id"] = userId;
            builder["Password"] = hashedPassword;
            builder["MultipleActiveResultSets"] = true;
            return builder.ConnectionString;
        }

    }
}
