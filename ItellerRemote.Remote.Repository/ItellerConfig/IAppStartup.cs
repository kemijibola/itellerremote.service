﻿using Owin;

namespace ItellerRemote.Remote.Repository.ItellerConfig
{
    public interface IAppStartup
    {
        void ConfigureAuth(IAppBuilder app);
    }
}
