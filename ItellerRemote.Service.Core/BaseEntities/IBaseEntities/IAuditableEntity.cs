﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItellerRemote.Service.Core
{
    public interface IAuditableEntity
    {
        DateTime CreatedOn { get; set; }
        int CreatedById { get; set; }
        DateTime UpdatedOn { get; set; }
        int UpdatedById{ get; set; }
    }
}
