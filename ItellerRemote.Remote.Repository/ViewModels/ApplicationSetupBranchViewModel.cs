﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItellerRemote.Service.Core;

namespace ItellerRemote.Remote.Repository.ViewModels
{
    public class ApplicationSetupBranchViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string SortCode { get; set; }
        public string StateCode { get; set; }
        public int CreatedBy { get; set; }
    }
}
