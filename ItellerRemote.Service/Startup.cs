﻿using DryIoc;
using DryIoc.WebApi;
using ItellerRemote.Remote.Repository.BaseRepository;
using ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository;
using ItellerRemote.Service.Core;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using ItellerRemote.Remote.Repository.ViewModels;
using ItellerRemote.Service.ItellerConfig;
using ItellerRemote.Service.Models;
using Microsoft.Ajax.Utilities;

[assembly: OwinStartupAttribute(typeof(ItellerRemote.Service.Startup))]
namespace ItellerRemote.Service
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            IContainer container = new Container(rules => rules.WithoutThrowOnRegisteringDisposableTransient());

            RegisterServices(container);

            container = container.WithWebApi(config, throwIfUnresolved: t => t.IsController());


            app.UseWebApi(config);
            //app.UseErrorPage(ErrorPageOptions.ShowAll);

            ConfigureAuth(app);
        }

        public static void RegisterServices(IRegistrator registrator)
        {

            registrator.Register(typeof(IRepository<,>), typeof(Repository<,>), Reuse.Singleton);
            
            registrator.Register(typeof(IApplicationSetUpBranch), typeof(ApplicationSetupBranchRepository), Reuse.Singleton);

            //registrator.Register<CurrentUserConfig>(Reuse.Singleton);
            //registrator.RegisterDelegate<UserViewModel>(r =>
            //        r.Resolve<CurrentUserConfig>().GetCurrentUser(),
            //    Reuse.Singleton);

            registrator.Register<CurrentUserConfig>(Reuse.Singleton);

            registrator.RegisterDelegate<UserViewModel>(r =>
                    r.Resolve<CurrentUserConfig>().GetCurrentUser(),
                Reuse.Singleton);

            registrator.Register(typeof(IUserRepository), typeof(UserRepository), Reuse.Singleton);
            registrator.Register(typeof(ICoreRepository), typeof(CoreRepository), Reuse.Singleton);
           
            //registrator.Register<CurrentUserConfig>(Reuse.Singleton);
            //registrator.Register<UserViewModel>(
            //    Made.Of(_ => ServiceInfo.Of<CurrentUserConfig>(), config => config.GetCurrentUser()),
            //    Reuse.InCurrentScope);


            registrator.Register<ITellerRemoteDBContext>(made: Made.Of(() => new ITellerRemoteDBContext()), reuse: Reuse.Singleton);
        }
    }
}
