﻿using ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository;
using ItellerRemote.Service.Controllers.BaseController;
using ItellerRemote.Service.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ItellerRemote.Remote.Repository.ViewModels;

namespace ItellerRemote.Service.Controllers
{
    [RoutePrefix("api/applicationsetup")]
    public class ApplicationSetupBranchController : ITellerBaseController
    {
        public ApplicationSetupBranchController(IApplicationSetUpBranch branchRepository,UserViewModel currentUser)
            :base(currentUser)
        {
            _branchRepository = branchRepository;
           // _currentUser = currentUser;
            //_currentUserId = currentUser.UserId;
        }

        private readonly IApplicationSetUpBranch _branchRepository;
        private int _currentUserId = 0;
        //private UserViewModel _currentUser;

        [HttpGet]
        [Route("branches")]
        public async Task<HttpResponseMessage> GetBranches()
        {
            var result = await _branchRepository.GetBranchesAsync();
            var currentUserId = CurrentUser.UserId;

            //var objResult = new {}
            //var response = Request.CreateResponse(HttpStatusCode.OK, result);
           // return new HttpResponseMessage(HttpStatusCode.OK);
            return ResponseMessage(HttpStatusCode.OK, "01", "Successful", result);

            //if (User.Identity.IsAuthenticated)
            //{
            //    var result = await _branchRepository.GetBranchesAsync();
            //    var response = Request.CreateResponse(HttpStatusCode.OK, result);
            //    return ResponseMessage(HttpStatusCode.OK, "01", "Successful", response);
            //}
            //else
            //    return ResponseMessage(HttpStatusCode.OK, "00", "You are not authorized to view this resource.", null);

        }

        [HttpPost]
        [Route("branch/create")]
        public async Task<HttpResponseMessage> Create(ApplicationSetupBranchViewModel branchViewModel)
        {
            var result = await _branchRepository.CreateBranch(branchViewModel);
            return ResponseMessage(HttpStatusCode.OK, "01", "Successful", result);
        }

        //[HttpGet]
        //[Route("branches/{id}")]
        //public async Task<HttpResponseMessage> GetBranchById(int BranchId)
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        var result = await _branchRepository.ge
        //    }
        //    return ResponseMessage(HttpStatusCode.OK, "00", "You are not authorized to view this resource.", null);
        //}

    }
}
