﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using ItellerRemote.Remote.Repository.ViewModels;

namespace ItellerRemote.Service.Controllers.BaseController
{
    public class ITellerBaseController : ApiController
    {
        public ITellerBaseController(UserViewModel currentUser)
        {
            CurrentUser = currentUser;
        }
        public UserViewModel CurrentUser;
        public HttpResponseMessage ResponseMessage(HttpStatusCode httpStatusCode, string statusCode, string message, object data)
        {
            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                HttpStatusCode = httpStatusCode,
                StatusCode = statusCode,
                Message = message,
                Data = data
            });
        }
    }
}
