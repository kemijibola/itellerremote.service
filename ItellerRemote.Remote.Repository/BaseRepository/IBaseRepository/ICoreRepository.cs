﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItellerRemote.Remote.Repository.ViewModels;

namespace ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository
{
    public interface ICoreRepository
    {
        Task<UserViewModel> GetUserByUsername(string username);
    }
}
