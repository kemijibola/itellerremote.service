﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItellerRemote.Service.Core
{
    [MetadataType(typeof(Security_UserBranchesMetadata))]
    public partial class Security_UserBranches : AuditableEntity<int>
    {

    }

    public class Security_UserBranchesMetadata : AuditableEntity<int>
    {
        public int Security_UsersId { get; set; }

        [Display(Name = "Branch")]
        [Required(ErrorMessage = "Branch is Required")]
        public int ApplicationSetup_BranchId { get; set; }
    }
}
