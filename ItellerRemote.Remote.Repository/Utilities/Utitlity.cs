﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ItellerRemote.Remote.Repository.Utilities
{
    public class Utitlity
    {

        public static string GenerateConnectionstring(string dbServerIP, string databaseName, string userId, string hashedPassword)
        {
            var builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
            builder["Server"] = dbServerIP;
            builder["integrated Security"] = false;
            builder["Database"] = databaseName;
            builder["User Id"] = userId;
            builder["Password"] = hashedPassword;
            builder["MultipleActiveResultSets"] = true;
            return builder.ConnectionString;
        }
        public static string NewGUID()
        {
            return Guid.NewGuid().ToString();
        }
        public static string Encrypt(string clearText)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(ApplicationsConstant.EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }

            return clearText;
        }
        public static string Decrypt(string cipherText)
        {
            var cipherBytes = Convert.FromBase64String(cipherText);
            using (var encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(ApplicationsConstant.EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        static string serverIp = GetConfigValue("Server");
        static string dbName = GetConfigValue("DBName");
        static string userId = GetConfigValue("UserID");
        static string password = GetConfigValue("Password");

        public static string GetConnectionString()
        {
            return GenerateConnectionstring(serverIp, dbName, userId, password);
        }

        private static string GetConfigValue(string _key)
        {
            return ConfigurationManager.AppSettings[_key];
        }
    }
}
