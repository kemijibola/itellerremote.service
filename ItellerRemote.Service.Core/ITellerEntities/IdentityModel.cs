﻿using ItellerRemote.Service.Core;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ItellerRemote.Service.Core
{
    
    public class Security_Users : IdentityUser<int, Securty_UserLogin, Security_UserRole, Security_UserClaim>, IEntity<int>
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<Security_Users, int> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public Security_Users()
        {
                UserBranches = new HashSet<Security_UserBranches>();
        }

        public int LoginCount { get; set; }
        public bool Status { get; set; }
        public DateTime? LastLogin { get; set; }
        public int CreatedById { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int UpdatedById { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public virtual ICollection<Security_UserBranches> UserBranches { get; set; }

    }

    public class Securty_UserLogin : IdentityUserLogin<int>
    {
    }

    public class Role : IdentityRole<int, Security_UserRole>
    {
        public Role() { }

        public int RoleTypeID { get; set; }
        public string Description { get; set; }  // Custom description field on roles
        public int CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public virtual RoleType RoleType { get; set; }

    }
    //public class Role : IdentityRole<int, Security_UserRole>
    //{
    //    public int RoleTypeID { get; set; }
    //    public string Description { get; set; }  // Custom description field on roles
    //    public int CreatedBy { get; set; }
    //    public DateTime? CreatedOn { get; set; }
    //    public int UpdatedBy { get; set; }
    //    public DateTime? UpdatedOn { get; set; }
    //    public virtual RoleType RoleType { get; set; }

    //}

    public class RoleType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }

    public class Security_UserRole : IdentityUserRole<int>
    {
        public virtual Role Role { get; set; }
    }

    public class Security_UserClaim : IdentityUserClaim<int>
    {
    }

    public class CustomUserStore : UserStore<Security_Users, Role, int,
    Securty_UserLogin, Security_UserRole, Security_UserClaim>
    {
        public CustomUserStore(ITellerRemoteDBContext context)
            : base(context)
        {
        }
    }

    public class CustomRoleStore : RoleStore<Role, int, Security_UserRole>
    {
        public CustomRoleStore(ITellerRemoteDBContext context)
            : base(context)
        {
        }
    }
}
