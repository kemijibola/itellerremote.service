﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ItellerRemote.Service.Core;

namespace ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository
{
    public interface IRepository<TEntity, TPrimaryKey> where TEntity : IEntity<TPrimaryKey>
    {

        #region Get/Select
        IQueryable<TEntity> GetAll();
        List<TEntity> GetAllList();
        Task<List<TEntity>> GetAllListAsync();
        #endregion

        #region SaveChanges
        //int SaveChanges();
        Task<TEntity> SaveChangesAsync(TEntity entity);
        //void Save();
        Task<int> SaveAsync();

        #endregion

    }
}
