﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItellerRemote.Service.Core
{
    [MetadataType(typeof(ApplicationSetup_BranchMetadata))]
    public partial class ApplicationSetup_Branch  : AuditableEntity<int>
    {

    }
    public class ApplicationSetup_BranchMetadata : AuditableEntity<int>
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }

        [Display(Name = "Branch code")]
        [Required(ErrorMessage = "Branch code is Required")]
        public string Code { get; set; }

        [Display(Name = "Branch address")]
        [Required(ErrorMessage = "Branch address is Required")]
        public string Address { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is Required")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Sort code")]
        [Required(ErrorMessage = "Sort code is Required")]
        public string SortCode { get; set; }

        [Display(Name = "State code")]
        [Required(ErrorMessage = "State code is Required")]
        public string StateCode { get; set; }
    }
}
