﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItellerRemote.Service.Core;

namespace ItellerRemote.Remote.Repository.ViewModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {
                //useri
        }
        public  int UserId { get; set; }
        public string Username { get; set; }
        public string LogoPath { get; set; }
        //public virtual ICollection<Security_UserRole> UserIsInRoles { get; set; }
        //public virtual ICollection<Security_UserBranches> UserIsInBranches { get; set; }
        public virtual List<UserRoles> UserIsInRoles { get; set; }
        public virtual List<UserBranches> UserIsInBranches { get; set; }
    }

    public class UserRoles
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }

    public class UserBranches
    {
        public int BranchId { get; set; }
        public string BranchName { get; set; }
    }
}
