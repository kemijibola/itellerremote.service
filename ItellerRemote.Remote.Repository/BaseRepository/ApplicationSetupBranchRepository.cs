﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository;
using ItellerRemote.Remote.Repository.ViewModels;
using ItellerRemote.Service.Core;

namespace ItellerRemote.Remote.Repository.BaseRepository
{
    public class ApplicationSetupBranchRepository : IApplicationSetUpBranch
    {
        public ApplicationSetupBranchRepository(IRepository<ApplicationSetup_Branch,int> branchRepository)
        {
            //_context = context;
            _branchRepository = branchRepository;
        }
        //private readonly ITellerRemoteDBContext _context;
        private readonly IRepository<ApplicationSetup_Branch,int> _branchRepository;

        public async Task<bool> CreateBranch(ApplicationSetupBranchViewModel branchModel)
        {
            try
            {
                if(branchModel == null)
                    throw new Exception("Fields are required");

                var objBranch = new ApplicationSetup_Branch
                {
                    Name = branchModel.Name,
                    Code = branchModel.Code,
                    Address = branchModel.Address,
                    Email = branchModel.Email,
                    SortCode = branchModel.SortCode,
                    StateCode = branchModel.StateCode,
                    CreatedById = branchModel.CreatedBy,
                    CreatedOn =  DateTime.UtcNow,

                };

                await _branchRepository.SaveChangesAsync(objBranch);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }
        public async Task<List<ApplicationSetupBranchViewModel>> GetBranchesAsync()
        {
            try
            {
                return await _branchRepository.GetAll().Select
                    ( x => new ApplicationSetupBranchViewModel
                    {
                        ID = x.Id,
                        Name = x.Name,
                        Code = x.Code,
                        Address = x.Address,
                        Email = x.Email,
                        SortCode = x.SortCode,
                        StateCode = x.StateCode
                    })
                    .ToListAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
