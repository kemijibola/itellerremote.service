﻿using iTellerRemote.Service.Core.iTellerEntities;
using ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITellerRemote.Service.Business
{
    public class ApplicationSetupBranch
    {
        public ApplicationSetupBranch(IRepository<ApplicationSetup_Branch> branchRepository)
        {
            _branchRepository = branchRepository;
        }

        private IRepository<ApplicationSetup_Branch> _branchRepository;

        public async Task<ICollection<ApplicationSetup_Branch>> GetBranches()
        {
            var result = await _branchRepository.GetAll();
            return result;

        }
    }
}
