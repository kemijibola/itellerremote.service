﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository;
using ItellerRemote.Remote.Repository.Utilities;
using ItellerRemote.Remote.Repository.ViewModels;
using ItellerRemote.Service.Core;
using ItellerRemote.Service.ItellerConfig;
using Microsoft.AspNet.Identity.Owin;

namespace ItellerRemote.Remote.Repository.BaseRepository
{
    public class UserRepository : IUserRepository
    {
        public UserRepository(ITellerRemoteDBContext context, UserViewModel user)
        {
            _context = context;
            _currentUserId = user.UserId;
        }

        private  readonly ITellerRemoteDBContext _context;
        private readonly int _currentUserId = 0;

        public async Task<bool> CreateUser(RegisterUserViewModel userViewModel)
        {
            try
            {
                var objUser = new Security_Users();
                objUser.UserName = userViewModel.Username;
                objUser.AccessFailedCount = 0;
                objUser.LoginCount = 0;
                objUser.Status = true;
                objUser.LockoutEnabled = false;
                objUser.CreatedById = _currentUserId;
                objUser.CreatedOn = DateTime.UtcNow;
                objUser.Email = userViewModel.Email;

                var key = SqlPasswordHasher.GenerateSalt();
                var hashSalt = SqlPasswordHasher.GetGenerateSalt(key);

                objUser.PasswordHash = SqlPasswordHasher.GenerateHMAC(userViewModel.Password, key);
                objUser.SecurityStamp = hashSalt;
                objUser.PhoneNumber = userViewModel.PhoneNumber;
                objUser.PhoneNumberConfirmed = false;
                objUser.EmailConfirmed = false;
                objUser.TwoFactorEnabled = false;

                var objResult = _context.Entry<Security_Users>(objUser).State = EntityState.Added;
                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
      
    }
}
