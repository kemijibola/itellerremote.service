﻿using ItellerRemote.Remote.Repository.Utilities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ItellerRemote.Service.Core
{
    public class ITellerRemoteDBContext : IdentityDbContext<Security_Users, Role, int, Securty_UserLogin, Security_UserRole, Security_UserClaim>
    {
        public ITellerRemoteDBContext()
        : base("iTellerCoreContext")
        {
            this.Database.Connection.ConnectionString = Utitlity.GetConnectionString();
        }

        //static string _connectionstring = string.Empty;

        //static string GetConnectionstring()
        //{
        //    return _connectionstring;
        //}

        //public ITellerRemoteDBContext() : base(GetConnectionstring()) { }

        //public ITellerRemoteDBContext(string connectionstring)
        //    : base(connectionstring)
        //{
        //    _connectionstring = connectionstring;
        //    this.Database.Connection.ConnectionString = _connectionstring;

        public virtual DbSet<Security_RoleType> Security_RoleType { get; set; }
        public virtual DbSet<Security_UserRole> Security_UserRoles { get; set; }
        public virtual DbSet<Security_UserBranches> Security_UserBranches { get; set; }
        public virtual DbSet<ItellerRemote.Service.Core.ApplicationSetup_Branch> ApplicationSetup_Branch { get; set; }
        //public virtual DbSet<ApplicationSetup_CompanyInformation> ApplicationSetup_CompanyInformation { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Security_UserLogins>().HasKey<int>(l => l.UserId);
            modelBuilder.Entity<Security_UserRole>().HasKey(r => new { r.RoleId, r.UserId });

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Security_Users>().ToTable("Security_Users");
            modelBuilder.Entity<Role>().ToTable("Security_Roles");
            modelBuilder.Entity<Security_UserRole>().ToTable("Security_UserRoles");

            // disable table pluralized names. 
            modelBuilder.Conventions.Add<PluralizingTableNameConvention>();
        }

        public static ITellerRemoteDBContext Create()
        {
            return new ITellerRemoteDBContext();
        }
    }
}
