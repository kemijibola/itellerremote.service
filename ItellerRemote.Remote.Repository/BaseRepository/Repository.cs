﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ItellerRemote.Remote.Repository.BaseRepository.IBaseRepository;
using ItellerRemote.Service.Core;

namespace ItellerRemote.Remote.Repository.BaseRepository
{
    public class Repository<TEntity, TPrimaryKey> : IRepository<TEntity, TPrimaryKey> where TEntity : class, IEntity<TPrimaryKey>
    {

        private readonly ITellerRemoteDBContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public Repository(ITellerRemoteDBContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        #region Get/Select
        public IQueryable<TEntity> GetAll()
        {
            return _dbSet.AsQueryable();
        }

        public List<TEntity> GetAllList()
        {
            return _dbSet.ToList();
        }

        public Task<List<TEntity>> GetAllListAsync()
        {
            return Task.FromResult(GetAllList());
        }

        #endregion

        #region SaveChanges
        public async Task<TEntity> SaveChangesAsync(TEntity entity)
        {
            _dbSet.Add(entity);
            await SaveAsync();
            return entity;
        }

        public async Task<int> SaveAsync()
        {

               return await _context.SaveChangesAsync();
        }
        #endregion

    }
}
